”Humidus” är latin och betyder fukt Humidus är ett tjänsteföretag med mångårig erfarenhet inom fukt och energibranschen.

När det händer, om det händer. Men, det ska inte behöva hända. 

Humidus Avfuktning arbetar förebyggande med besiktning och rådgivning vad gäller fukt- och vattenskador samt med redan uppkomna skador i boendemiljö.

Ett professionelt arbetsätt, mångårig erfarenhet och stor kunskap inom företaget garanterar kunden den bästa tänkbara hjälp - både före och efter det att skadan är skedd.

När bara det bästa är gott nog är det enkelt att kontakta Humidus.